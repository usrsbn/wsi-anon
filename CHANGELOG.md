# Changelog

## 0.3.11

- refactor code
- separate source files by format
- implement expandable format approach

## 0.3.10

- updated README.md

## 0.3.9

- implement windows support for all data formats

## 0.3.8 

- full isyntax support

## 0.3.7

- Remove ScanDataLayer_WholeSlide and ScanDataLayer_SlidePreview
- Remove more metadata

## 0.3.6

- Updated Readme

## 0.3.5

- Added ventana anonymization

## 0.3.4

- Added integration tests

## 0.3.3

- Remove metadata from tiff-based files

## 0.3.2

- Remove metadata from tiff-based files

## 0.3.1

- Basic support for Leica Aperio GT450
- Bugfix for Mirax
- Added code checks to CI

## 0.3.0

- Configurable chunk size

## 0.2.0

- Result propagation
- Fix filename and string handling

## 0.1.0

- Init
